package com.logitech.upnp;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author iusenko
 */
public class SearchingRequestTest {

    private static final String rawRequest = "M-SEARCH * HTTP/1.1\r\n"
            + "Man: \"ssdp:discover\"\r\n"
            + "Mx: 3\r\n"
            + "Host: 239.255.255.250:1900\r\n"
            + "St: ssdp:all\r\n"
            + "\r\n";

    @Test
    public void testToString() {
        assertEquals(rawRequest, new SearchingRequest().toString());
    }
}
