package com.logitech.upnp;

import org.junit.Test;
import java.util.Map;
import static org.junit.Assert.*;

/**
 *
 * @author iusenko
 */
public class ResponseParserTest {

    private String rawResponse = "HTTP/1.1 200 OK\r\n"
            + "LOCATION: http://192.168.147.58:52221/\r\n"
            + "EXT:\r\n"
            + "SERVER: WINDOWS, UPnP/1.0, Intel MicroStack/1.0.1423\r\n"
            + "USN: uuid:LogiSecurityDevice_00-12-AB-1B-34-A6::urn:upnp-logitech-com:service:SecurityDeviceControl:1\r\n"
            + "CACHE-CONTROL: max-age=1800\r\n"
            + "ST: urn:upnp-logitech-com:service:SecurityDeviceControl:1\r\n"
            + "\r\n";

    @Test
    public void testParseHeader() {
        String location = "http://192.168.147.58:52221/";
        String usn = "uuid:LogiSecurityDevice_00-12-AB-1B-34-A6::urn:upnp-logitech-com:service:SecurityDeviceControl:1";

        ResponseParser p = new ResponseParser(null, null);
        Map<String, String> header = p.parseHeader(rawResponse.getBytes());
        assertEquals(location, header.get(ResponseParser.LOCATION_KEY));
        assertEquals(usn, header.get(ResponseParser.USN_KEY));
    }

    @Test
    public void testIsLogitechDevice() {
        String usn = "uuid:LogiSecurityDevice_00-12-AB-1B-34-A6::urn:upnp-logitech-com:service:SecurityDeviceControl:1";
        ResponseParser p = new ResponseParser(null, null);
        assertTrue(p.isLogitechDevice(usn));
        assertFalse(p.isLogitechDevice(""));
        assertFalse(p.isLogitechDevice(null));
    }
}
