package com.logitech.upnp;

import java.net.DatagramPacket;
import java.net.MulticastSocket;

/**
 *
 * @author iusenko
 */
public class DeviceDiscoverer {

    public static final int TIME_TO_LIVE = 4;
    private MulticastSocket multicastSocket;
    private IDeviceListener deviceListener;
    private RequestSender sender;
    private ResponseReceiver receiver;

    public DeviceDiscoverer(IDeviceListener deviceListener) {
        if (deviceListener == null) {
            throw new IllegalArgumentException();
        }
        this.deviceListener = deviceListener;
    }

    private void closeSocket() {
        multicastSocket.close();
        multicastSocket = null;
    }

    public void start() throws Exception {
        multicastSocket = new MulticastSocket(UpnpUtils.UPNP_MULTICAST_PORT);
        multicastSocket.joinGroup(UpnpUtils.getMulticastAddress());
        multicastSocket.setTimeToLive(TIME_TO_LIVE);
        //multicastSocket.setSoTimeout(500);

        startReceiverThread();
        startSenderThread();
    }

    private void startReceiverThread() {
        receiver = new ResponseReceiver(multicastSocket, deviceListener);
        receiver.start();
    }

    private void stopReceiverThread() {
        boolean retry = true;
        receiver.setRunning(false);
        receiver.interrupt();
        while (retry) {
            try {
                receiver.join();
                retry = false;
            } catch (InterruptedException ex) {
                // ignore it
            }
        }
    }

    private void startSenderThread() {
        sender = new RequestSender(multicastSocket);
        sender.start();
    }

    private void stopSenderThread() {
        boolean retry = true;
        sender.setRunning(false);
        while (retry) {
            try {
                sender.join();
                retry = false;
            } catch (InterruptedException ex) {
                // ignore it
            }
        }
    }

    public void stop() {
        closeSocket();
        stopSenderThread();
        stopReceiverThread();
    }

    /**
     * Thread which periodically sends requests for discovering devices.
     */
    private class RequestSender extends Thread {

        private static final int SLEEP_TIMEOUT = 1000;
        private MulticastSocket socket;
        private boolean running;

        public RequestSender(MulticastSocket socket) {
            this.socket = socket;
            this.running = true;
        }

        public void setRunning(boolean running) {
            this.running = running;
        }

        @Override
        @SuppressWarnings("SleepWhileInLoop")
        public void run() {
            SearchingRequest request = new SearchingRequest();
            DatagramPacket packet = request.createMulticastDatagramPacket();

            while (running) {
                try {
                    if (socket != null && !socket.isClosed()) {
                        socket.send(packet);
                    }
                    sleep(SLEEP_TIMEOUT);
                } catch (Exception ex) {
                    break;
                }
            }
        }
    }

    private class ResponseReceiver extends Thread {

        public static final int MAX_DATAGRAM_BYTES = 640;
        private MulticastSocket socket;
        private IDeviceListener deviceListener;
        private boolean running;

        public ResponseReceiver(MulticastSocket socket, IDeviceListener deviceListener) {
            this.socket = socket;
            this.deviceListener = deviceListener;
            this.running = true;
        }

        public void setRunning(boolean running) {
            this.running = running;
        }

        @Override
        public void run() {
            while (running) {

                try {
                    byte[] buf = new byte[MAX_DATAGRAM_BYTES];
                    DatagramPacket datagram = new DatagramPacket(buf, buf.length);
                    // thread can be interrupted from external class
                    socket.receive(datagram);
                    new ResponseParser(deviceListener, datagram).start();
                } catch (Exception ex) {
                    break;
                }
            }
        }
    }
}
