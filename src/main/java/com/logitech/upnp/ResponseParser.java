package com.logitech.upnp;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.DatagramPacket;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author iusenko
 */
public class ResponseParser extends Thread {

    private static int responseCounter = 0;
    private static final int BUFFER_SIZE = 500;
    protected static final String USN_KEY = "USN";
    protected static final String LOCATION_KEY = "LOCATION";
    protected static final String LOGITECH_DEVICE_ID = "upnp-logitech-com";
    private IDeviceListener deviceListener;
    private DatagramPacket packet;
    private BufferedReader reader;

    public ResponseParser(IDeviceListener deviceListener, DatagramPacket packet) {
        this.deviceListener = deviceListener;
        this.packet = packet;
        setName("Response parser " + (responseCounter++));
    }

    @Override
    public void run() {
        Map<String, String> header = parseHeader(packet.getData());

        String location = header.get(LOCATION_KEY);
        String usn = header.get(USN_KEY);

        if (!isLogitechDevice(usn) || location == null) {
            return;
        }
        
        String hostAddress = packet.getAddress().getHostAddress();
        UpnpDevice device = new UpnpDevice(hostAddress);
        device.setLocation(location);
        String name = UpnpUtils.readFiendlyNameFromUrl(location);
        device.setName(name);

        deviceListener.device(device);
    }

    protected boolean isLogitechDevice(String usnValue) {
        return usnValue != null && usnValue.contains(LOGITECH_DEVICE_ID);
    }

    protected Map<String, String> parseHeader(byte[] data) {
        Map<String, String> header = new HashMap<String, String>();
        reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(data)), BUFFER_SIZE);
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                int index = line.indexOf(":");
                if (index == -1) {
                    continue;
                }

                String key = line.substring(0, index).trim();
                String value = line.substring(index + 1).trim();
                header.put(key, value);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        safeClose(reader);
        return header;
    }

    private void safeClose(Reader reader) {
        if (reader == null) {
            return;
        }

        try {
            reader.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
