package com.logitech.upnp;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author iusenko
 */
public class SearchingRequest extends AbstractRequest {

    public SearchingRequest() {
        super(UpnpRequestMethod.MSEARCH);
    }

    @Override
    protected Map<UpnpHeader, String> getHeaders() {
        Map<UpnpHeader, String> headers = new LinkedHashMap<UpnpHeader, String>(4);
        headers.put(UpnpHeader.MAN, "\"ssdp:discover\"");
        headers.put(UpnpHeader.MX, "3");
        headers.put(UpnpHeader.HOST, "239.255.255.250:1900");
        headers.put(UpnpHeader.ST, "ssdp:all");
        return headers;
    }

    @Override
    protected String getReuestMethodValue() {
        return "* HTTP/1.1";
    }
}
