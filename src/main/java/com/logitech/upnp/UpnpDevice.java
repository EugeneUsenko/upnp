package com.logitech.upnp;

/**
 *
 * @author iusenko
 */
public class UpnpDevice {

    private String hostAddress;
    private String name;
    private String location;

    public UpnpDevice(String hostAddress) {
        this.hostAddress = hostAddress;
    }   

    public String getHostAddress() {
        return hostAddress;
    }

    public void setHostAddress(String hostAddress) {
        this.hostAddress = hostAddress;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UpnpDevice other = (UpnpDevice) obj;
        if ((this.hostAddress == null) ? (other.hostAddress != null) : !this.hostAddress.equals(other.hostAddress)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + (this.hostAddress != null ? this.hostAddress.hashCode() : 0);
        return hash;
    }    
    
}
