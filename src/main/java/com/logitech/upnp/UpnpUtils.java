package com.logitech.upnp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.InetAddress;
import java.net.URL;

/**
 *
 * @author iusenko
 */
public final class UpnpUtils {

    public static final int BUFFER_SIZE = 1024;
    public static final int UPNP_MULTICAST_PORT = 1900;
    public static final String IPV4_UPNP_MULTICAST_GROUP = "239.255.255.250";
    private static InetAddress MULTICAST_ADDRESS;

    public static synchronized InetAddress getMulticastAddress() {
        if (MULTICAST_ADDRESS == null) {
            try {
                MULTICAST_ADDRESS = InetAddress.getByName(IPV4_UPNP_MULTICAST_GROUP);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
        return MULTICAST_ADDRESS;
    }

    public static String readFiendlyNameFromUrl(String url) {
        String value = "Undefined";
        if (url == null || "".equals(url.trim())) {
            return value;
        }

        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(new URL(url).openStream()), BUFFER_SIZE);
            String line;
            while ((line = in.readLine()) != null) {
                if (line.contains("<friendlyName>")) {
                    int startIndex = line.indexOf("<friendlyName>") + "<friendlyName>".length();
                    int endIndex = line.indexOf("</friendlyName>");
                    if (endIndex != -1) {
                        return line.substring(startIndex, endIndex);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            safeClose(in);
        }
        return value;

    }

    private static void safeClose(Reader reader) {
        if (reader == null) {
            return;
        }

        try {
            reader.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
