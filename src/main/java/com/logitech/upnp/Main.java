package com.logitech.upnp;

import java.util.HashSet;
import java.util.Set;

/**
 * Hello world!
 *
 */
public class Main {

    public static void main(String[] args) throws Exception {
        final Set<UpnpDevice> devices = new HashSet<UpnpDevice>();

        DeviceDiscoverer deviceDiscoverer = new DeviceDiscoverer(new IDeviceListener() {

            public void device(UpnpDevice device) {
                if (!devices.contains(device)) {
                    devices.add(device);
                    System.out.println();
                    System.out.println("IP: " + device.getHostAddress());
                    System.out.println("Location: " + device.getLocation());
                    System.out.println("Name: " + device.getName());
                }
            }
        });
        deviceDiscoverer.start();

    }
}
