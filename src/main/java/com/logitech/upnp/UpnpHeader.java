package com.logitech.upnp;

/**
 * Enumeration which represent header keys of the UPNP request.
 * @author iusenko
 */
public enum UpnpHeader {

    MAN("Man"),
    MX("Mx"),
    ST("St"),
    HOST("Host");
    private String headerName;

    private UpnpHeader(String headerName) {
        this.headerName = headerName;
    }

    @Override
    public String toString() {
        return headerName;
    }
}
