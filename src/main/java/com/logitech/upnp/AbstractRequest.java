package com.logitech.upnp;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.Map;

/**
 *
 * @author iusenko
 */
public abstract class AbstractRequest {

    private static final String LINE_END = "\r\n";
    private UpnpRequestMethod requestMethod;

    public AbstractRequest(UpnpRequestMethod requestMethod) {
        this.requestMethod = requestMethod;
    }

    protected abstract Map<UpnpHeader, String> getHeaders();

    protected abstract String getReuestMethodValue();

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        // method
        sb.append(requestMethod).append(" ").append(getReuestMethodValue()).append(LINE_END);

        // headers
        Map<UpnpHeader, String> headers = getHeaders();
        for (UpnpHeader header : headers.keySet()) {
            String value = headers.get(header);
            sb.append(header).append(": ").append(value).append(LINE_END);
        }

        // end
        sb.append(LINE_END);

        return sb.toString();
    }

    public DatagramPacket createMulticastDatagramPacket() {
        byte[] data = toString().getBytes();
        InetAddress multicast = UpnpUtils.getMulticastAddress();
        int port = UpnpUtils.UPNP_MULTICAST_PORT;
        DatagramPacket packet = new DatagramPacket(data, data.length, multicast, port);
        return packet;
    }
}
