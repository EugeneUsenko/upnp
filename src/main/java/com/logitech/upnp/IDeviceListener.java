package com.logitech.upnp;

/**
 *
 * @author iusenko
 */
public interface IDeviceListener {

    void device(UpnpDevice device);
}
