package com.logitech.upnp;

/**
 * Enumeration which represents types of the UPNP requests.
 * @author iusenko
 */
public enum UpnpRequestMethod {

    MSEARCH("M-SEARCH");
    private String methodName;

    private UpnpRequestMethod(String methodName) {
        this.methodName = methodName;
    }

    @Override
    public String toString() {
        return methodName;
    }
}
